# -*- coding: utf-8 -*-
##############################################################################
#
#    product_ean13_validation_remove module for OpenERP, Remove Validation
#    Copyright (C) 2014 BizzAppDev (<http://bizzappdev.com>) Ruchir Shukla
#
#    This file is a part of product_ean13_validation_remove
#
#    product_ean13_validation_remove is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    product_ean13_validation_remove is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import product
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
